import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/sass/main.scss';

import AgeGate from "../components/age-gate"

import Footer from "../components/footer"

export default () => (
    <div className="d-flex flex-column h-100 privacy-policy">
      <div className="container mt-5">
        <div className="content-container">
        <div className="header">
          <h1>Privacy Policy</h1>
          <h2>LAST UPDATED JUNE 2018</h2>
        </div>
                  <h3 className="legal-headline">1. GENERAL</h3>
          <p>This Privacy Policy sets out the privacy policies and practices for High Park Holdings Ltd. (High Park) with respect to High Park’s collection, use, and disclosure of personal information through its website [marleyriseup.ca] (the “Site”). <br/><br/> In this Privacy Policy, “personal information” means any information about an identifiable individual, as further defined under applicable Canadian laws. <br/><br/> High Park may update this Privacy Policy from time to time.</p>
                  <h3 className="legal-headline">2. CONSENT</h3>
          <p>An individual’s consent is required for the collection and proposed use of personal information. Consent can be either express or implied. Express consent can be given orally, electronically or in writing. Implied consent is consent that can reasonably be inferred from an individual’s action or inaction. An individual’s consent is required before confidential information is released to outside parties.</p>
                  <h3 className="legal-headline">3. COLLECTION OF PERSONAL INFORMATION</h3>
          <p>High Park and its agents and representatives collect personal information from visitors to the site (an “Individual”) when an Individual accesses the Site or submits information to High Park, and may from time to time collect additional personal information in the course of providing products and services. Personal information we collect include: <br/><br/> An Individual’s name, date of birth, email, and other contact information; <br/><br/> Information about an Individual’s preferences and feedback on our products and services; <br/><br/> Demographic and profile data; and <br/><br/> Such other information we may collect with your consent or as permitted or required by law or regulation.</p>
                  <h3 className="legal-headline">4. USE OF PERSONAL INFORMATION</h3>
          <p>High Park generally uses personal information for the following purposes: <br/><br/>
As is reasonably required to provide the requested services, products, or information; <br/><br/>
Maintaining business records for reasonable periods; <br/><br/>
Analyzing, developing and improving its product services and offerings; <br/><br/>
Providing updates, alerts, bulletins, or other similar communications about our products and services; <br/><br/>
If you choose this option, providing news, promotions and other communications about products and services we think you may be interested in; <br/><br/>
Tailoring each Individual’s experience at the Site and to display the Site content according to the Individual’s preferences; and <br/><br/>
Otherwise with consent or as permitted by law. <br/><br/>
From time to time, High Park may offer interactive services which allow the Individual to share information with other users of the Site, such as message boards, user reviews, or other similar functions. These features may permit the Individual to publish personal information about him or herself, or about another individual. By submitting such information, the Individual represents: (a) that the Individual consents to its collection, retention, and public disclosure on the Site; and (b) that the Individual has obtained the same consent from any other individuals, as applicable. <br/><br/>
High Park may use personal information for additional purposes that may be identified at or before the time that the information is collected, or as required by law. High Park will retain the personal information only as long as necessary for the purposes outlined in this Privacy Policy or as required or permitted by law.</p>
                  <h3 className="legal-headline">5. DISCLOSURE OF PERSONAL INFORMATION</h3>
          <p>High Park discloses personal information in the following circumstances: <br/><br/>
to regulatory bodies for the purposes of demonstrating compliance with applicable law; <br/><br/>
to law enforcement agencies for the purposes of investigating fraud or other offences, in connection with the prevention or investigation of suspected fraudulent activities or harm to individuals or property, to establish or exercise our legal rights or defend against legal claims, or in connection with an emergency that warrants use or disclosure of the information; <br/><br/>
to third party insurers, for billing and administrative purposes; <br/><br/>
to legal, financial, insurance, and other advisors in connection with the sale, reorganization, or management of all or part of its business or operations; <br/><br/>
as consented to by an Individual from time to time, including to fulfill any other purposes that are identified when the personal information is collected. <br/><br/>
High Park will not use or disclose personal information for purposes other than those for which it was collected, except with an Individual’s consent or as required or permitted by law. Consent may be express or implied, and given in writing by using or not using a check-off box, electronically, orally, or by the Individual’s conduct, such as purchase or use of the services or products.</p>
                  <h3 className="legal-headline">6. USE OR DISCLOSURE FOR RESEARCH PURPOSES</h3>
          <p>In order to improve its processes, products and service offerings, High Park may from time to time make use of aggregated and non-identifying information (“Aggregated Data”) for research purposes, such as to better understand the needs and wants of its customers and users. High Park may disclose such Aggregated Data, which will not personally identify any Individual, to its affiliates, agents, service providers and business partners for these purposes. <br/><br/>
From time to time, High Park may request an Individual’s consent to use or disclose personal information about the Individual for research purposes, including in relation to the Individual’s use of cannabis. For example, High Park may invite an Individual to complete a survey or to participate in a study, which may be conducted by High Park or by a third party. High Park will not use or disclose an Individual’s personal information for such research purposes without the Individual’s express consent, which may be withheld or denied without consequence to the use of any unrelated function of the Site, including access to any other products or services offered.</p>
                  <h3 className="legal-headline">7. STORAGE AND PROCESSING OUTSIDE OF CANADA</h3>
          <p>High Park may transfer personal information to outside agents or service providers, including affiliates of High Park acting in this capacity, that perform services on our behalf, for example information technology services, data hosting/processing services, or similar services, or otherwise to collect, use, disclose, store or process personal information on our behalf for the purposes described in this Privacy Policy. Some of these service providers or affiliates may be located outside of Canada, including in the United States, and your personal information may be collected, used, disclosed, stored and processed in the United States or elsewhere outside of Canada for the purposes described in this Privacy Policy. Reasonable contractual other measures we may take to protect your personal information while processed or handled by these service providers are subject to legal requirements in Canada, the United States and other foreign countries applicable to our affiliates, agents and service providers, for example lawful requirements to disclose personal information to government authorities in those countries. <br/><br/>
Any questions in this regard may be directed to High Park’s Privacy Officer (the “Privacy Officer”) using the contact information provided below.</p>
                  <h3 className="legal-headline">8. COOKIES AND IP ADDRESSES</h3>
          <p>High Park uses an Individual’s IP address to help identify an Individual, gather broad demographic information about users of the Site, diagnose problems with High Park’s systems, and administer the Sites. Depending on a user’s browser settings, the Site may use cookies (a small text file that is stored on a user’s computer for record-keeping purposes) to deliver content according to the Individual’s preferences and to save the Individual’s password so that the Individual is not required to re-enter it while the Individual uses the Site.</p>
                  <h3 className="legal-headline">9. THIRD PARTY ADVERTISING / ANALYTICS</h3>
          <p>We may partner with third party advertising services to present you with advertising on third party Web sites based on your previous interaction with the Site. The techniques our partners employ do not directly collect personal information, but could be used to collect identifiable information about your online activities over time and across different Web sites, including when you use the Site. <br/><br/>
We also implement Google Analytics Demographics and Interest Reporting, a display advertising feature from Google Analytics that allows us to review anonymous data regarding the gender, age and interests of visitors to the Site and adapt our content to better reflect the needs of our visitors. You can learn about Google Analytics’ Privacy Policy and use of cookies here. You can opt out of having your data used by Google Analytics by installing the Google Analytics opt-out browser add-on.</p>
                  <h3 className="legal-headline">10. SECURITY</h3>
          <p>The file containing Individual personal information will be maintained in High Park’s servers or those of its service providers. High Park’s employees, representatives or agents will have access to an Individual’s personal information as necessary in connection with their role. High Park protects against the loss, misuse, and alteration of personal information with security measures appropriate to the sensitivity of the information, including through the use of physical, organizational, and technological measures and appropriate training of employees.</p>
                  <h3 className="legal-headline">11. OPT-OUT</h3>
          <p>An Individual may opt out of receiving further communications by using the unsubscribe mechanism at the bottom of our email communications or by contacting the Privacy Officer using the contact information provided below.</p>
                  <h3 className="legal-headline">12. ACCESS AND CORRECTIONS</h3>
          <p>An Individual may contact the Privacy Officer, using the contact information provided below, to modify or correct any of his or her personal information that is under High Park’s control, or to request access to that information. An Individual may also direct a written complaint regarding compliance with this Privacy Policy to the Privacy Officer and, within a reasonable time upon receiving the written complaint, the Privacy Officer will conduct an investigation into the matter. Within a reasonable time of concluding the investigation, the Privacy Officer will respond to the complaint and, if appropriate, High Park will take appropriate measures necessary to rectify the source of the complaint.</p>
                  <h3 className="legal-headline">13. CONTACT</h3>
          <p>If an Individual has any questions about this Privacy Policy or High Park’s collection, use, disclosure, or retention of the Individual’s personal information, please contact the Privacy Officer as follows: <br/><br/>
Attention: Privacy Officer <br/><br/>
High Park Holdings Ltd.<br/> 495 Wellington St W, Unit 300<br/> Toronto, ON, M5V 1E9, Canada <br/><br/>
Email: <a href="mailto:legal@highparkcompany.com">legal@highparkcompany.com</a></p>
                <div className="footer">
          Rmdy Cannabis Company | High Park Company<br/> 300-495 Wellington St W | Toronto, ON, M‍5V 1E9, Canada
        </div>
    </div>
      </div>   
      <AgeGate/>     
      <Footer />
    </div>
  )