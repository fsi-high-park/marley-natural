import React from "react";

import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/sass/main.scss';

// import Header from "../components/header"
import Form from "../components/form"
import AgeGate from "../components/age-gate"
import Footer from "../components/footer"

export default () => (
    <div className="d-flex flex-column h-100">
        {/* <Header /> */}
        <Form />
        <AgeGate/>
        <Footer />
    </div>
  )