import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/sass/main.scss';

import AgeGate from "../components/age-gate"
import Footer from "../components/footer"

/*Assets*/
import white_logo from "../assets/images/marley-rise-up-logo-wht@2x.png"

export default () => (
    <div className="d-flex flex-column h-100">
        <div id="thank-you" className="py-5 text-center">
            <img className=" mb-4 mt-5" src={white_logo} alt="Rise Up Logo"/>
            <h1 className="mb-4 ">Thanks for signing up!</h1> 
        </div>
      <AgeGate/>     
      <Footer />
    </div>
  )