import React from "react"
export default class province extends React.Component {
    renderOptions(){
        let optionsDOM = [];
        let provinces = [
            {val:'AB', label:'Alberta'},
            {val:'BC', label:'British Columbia'},
            {val:'MB', label:'Manitoba'},
            {val:'NB', label:'New Brunswick'},
            {val:'NL', label:'Newfoundland and Labrador'},
            {val:'NT', label:'Northwest Territories'},
            {val:'NS', label:'Nova Scotia'},
            {val:'NU', label:'Nunavut'},
            {val:'ON', label:'Ontario'},
            {val:'PE', label:'Prince Edward Island'},
            {val:'QC', label:'Quebec'},
            {val:'SK', label:'Saskatchewan'},
            {val:'YT', label:'Yukon Territory'}];
        provinces.forEach((element, key) => {
            optionsDOM.push(<option key={key} value={element.val}>{element.label}</option>)
        })
        return optionsDOM;
    }
    render() {
        return(
            <select className="form-control" id={this.props.id} name={this.props.name} value={this.props.province} onChange={this.props.handleInputChange}>
                <option selected="true" disabled>Province/Territory</option>
                {this.renderOptions()}
            </select>
        )
    }
}