import React from "react"
export default class year extends React.Component {
    renderOptions(){
        let optionsDOM = [];

        for (let index = 1899; index <= 2019; index++) {
            optionsDOM.push(<option key={index} value={index}>{index}</option>)
        }
        return optionsDOM;
    }
    render() {
        return(
            <select className="form-control" id={this.props.id} name={this.props.name} value={this.props.year} onChange={this.props.handleInputChange}>
                <option selected="true" disabled>Year</option>
                {this.renderOptions()}
            </select>
        )
    }
}