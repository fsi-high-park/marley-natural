import React from "react"
export default class month extends React.Component {
    renderOptions(){
        let optionsDOM = [];
        let months = [
            {val:"1", label:'January'},
            {val:"2", label:'February'},
            {val:"3", label:'March'},
            {val:"4", label:'April'},
            {val:"5", label:'May'},
            {val:"6", label:'June'},
            {val:"7", label:'July'},
            {val:"8", label:'August'},
            {val:"9", label:'September'},
            {val:"10", label:'October'},
            {val:"11", label:'November'},
            {val:"12", label:'December'}
        ];
        months.forEach((element, key) => {
            optionsDOM.push(<option key={key} value={element.val}>{element.label}</option>)
        })
        return optionsDOM;
    }
    render() {
        return(
            <select className="form-control" id={this.props.id} name={this.props.name} value={this.props.month} onChange={this.props.handleInputChange}>
                <option selected="true" disabled>Month</option>
                {this.renderOptions()}
            </select>
        )
    }
}