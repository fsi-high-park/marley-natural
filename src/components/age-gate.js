import React from "react"

import Province from "./age-components/province"
import Month from "./age-components/month"
import Day from "./age-components/day"
import Year from "./age-components/year"
import Footer from "./footer"


/*Assets*/
import white_logo from "../assets/images/marley-rise-up-logo-wht@2x.png"

export default class form extends React.Component {
    state = {
        province:"",
        month:"",
        day:"",
        year:"",
        age:true,
        formErrors: {province: false, month: false, day: false, year: false, age: false}
    }
    componentDidMount(){
        let age = window.localStorage.getItem('age');
        if(!age){
            this.setState({age: false});            
            document.body.classList.add("age-gate-open");
        }else{
            this.setState({age: true});                        
            document.body.classList = "";
        }
    }
    handleInputChange = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
          [name]: value,
        })
    }
    handleSubmit = event => {
        event.preventDefault();
        let age_details = {
            province:this.state.province,
            month:this.state.month,
            day:this.state.day,
            year:this.state.year
        }
        //Create Array of age details to loop through
        const field_keys = Object.keys(age_details);
        var tempFormErrors = this.state.formErrors;
        var formValid = true;

        for(var field_key of field_keys){
            //Loop through form fields, if one is left empty flag it as an error in formErrors state object
            if(age_details[field_key] === ""){
                tempFormErrors[field_key] = true;
                formValid = false;
            }else{
                tempFormErrors[field_key] = false;
            }
        }
        // If we have errors stop here and set the error state object.
        this.setState({formErrors: tempFormErrors});
        if(!formValid){ return; }

        //Checking for legal age as per province
        var tempFormErrors = this.state.formErrors;
        var age = this.getAge(this.state.year+"/"+this.state.month+"/"+this.state.day);

        if(this.state.province === "AB" || this.state.province === "QC"){
            if(age < 18){
                tempFormErrors.age = true;
                formValid = false;
            }else{
                tempFormErrors.age = false;
            }
        }else{
            if(age < 19){
                tempFormErrors.age = true;
                formValid = false;
            }else{
                tempFormErrors.age = false;
            }
        }
        // Set the error state object.
        this.setState({formErrors: tempFormErrors});
        
        // Check if form is valid. If so save to local storage their age and hide age gate.
        if(formValid){
            window.localStorage.setItem('age', JSON.stringify(age_details));
            document.body.classList = "";
            //this.setState({age: true});
            window.location.reload();
            
        }
    } 
    isValid(field_key){
        if(this.state.formErrors[field_key]){
            return "error ";
        }else{
            return "";
        }
    }
    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    render() {
        return(
            <div id="age-gate">
                {!this.state.age ? (
                <div className="modal fade show" id="exampleModalCenter" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div className="modal-holder d-flex align-items-center flex-grow-1" role="document">
                      <div className="modal-content text-center">
                        <div className="modal-header">
                          <img className="mw-100" src={white_logo} alt="Rise Up Logo"/>
                        </div>
                        <div className="modal-body">
                            <p>It’s time to join the movement,<br/>but first let us know your date of birth.</p>
                            <form className="mt-5" onSubmit={this.handleSubmit}>
                                <div className="form-row ">
                                    <div className={"col-md-4 form-group " + this.isValid("month") + this.isValid("day") + this.isValid("year")}>
                                        <label htmlFor="lead_month">Month</label>
                                        <Month id="lead_month" name="month" handleInputChange={this.handleInputChange}/>
                                        <small className="error-message">*Select date of birth.</small>
                                    </div>
                                    <div className={"mb-4 mb-md-0 col-md-4 form-group " + this.isValid("day")}>
                                        <label htmlFor="lead_day">Day</label>
                                        <Day id="lead_day" month={this.state.month} name="day" handleInputChange={this.handleInputChange}/>
                                    </div>
                                    <div className={"mb-4 mb-md-0 col-md-4 form-group " + this.isValid("year")}>
                                        <label htmlFor="lead_year">Year</label>
                                        <Year id="lead_year" name="year" handleInputChange={this.handleInputChange}/>
                                    </div>
                                </div>
                                <div className={"form-group " + this.isValid("province")}>
                                    <label htmlFor="lead_province">Province</label>
                                    <Province id="lead_province" name="province" handleInputChange={this.handleInputChange}/>
                                    <small className="error-message">*Select your Province/Territory.</small>
                                </div>
                                <p className={"age-message "+this.isValid("age")}><small>Oops! Looks like you’re not old enough to enjoy.<br/>Come back when you’ve reached the legal age for cannabis consumption.</small></p>

                                <button type="submit" className="btn btn-primary mt-3">Enter</button>
                            </form>
                        </div>
                      </div>
                    </div>
                    <Footer />
                  </div>
                ) : (<p className="d-none">No age gate.</p>)}
            </div>
        )
    }
}