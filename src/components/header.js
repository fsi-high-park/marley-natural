import React from "react"
import Nav from "./nav"
export default () => (
    <header>
        <Nav/>
        <h1>This is a header.</h1>
    </header>
)