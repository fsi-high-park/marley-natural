import React from "react"
import axios from 'axios';
import { navigate } from "gatsby"


import Province from "../components/age-components/province"
import Month from "../components/age-components/month"
import Day from "../components/age-components/day"
import Year from "../components/age-components/year"

/*Assets*/
import white_logo from "../assets/images/marley-rise-up-logo-wht@2x.png"

export default class form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName:"",
            lastName:"",
            email:"",
            newsletter:"",
            month:"",
            day:"",
            year:"",
            province:"",
            local:"en",
            formErrors: { firstName:false, lastName:false, email:false, province: false, month: false, day: false, year: false, age: false},
            formValid: false
        }
    }
    componentDidMount(){
        let age = window.localStorage.getItem('age');
        if(age){
            age = JSON.parse(age);
            this.setState({month:age.month, day:age.day, year:age.year, province:age.province});
        }
    }
    handleInputChange = event => {
        const target = event.target
        const value = target.value
        const name = target.name
        this.setState({
          [name]: value,
        })
    }
    handleSubmit = event => {
        event.preventDefault();
        let form_details = {
            firstName:this.state.firstName,
            lastName:this.state.lastName,
            email:this.state.email,
            province:this.state.province,
            month:this.state.month,
            day:this.state.day,
            year:this.state.year
        }
        var formValid = this.validateForm(form_details);
        
        if(formValid){
            this.sendFormData();
        }
    }
    validateForm(form_details){
        //Create Array of age details to loop through
        const field_keys = Object.keys(form_details);
        var tempFormErrors = this.state.formErrors;
        var formValid = true;
        
        for(var field_key of field_keys){
            //Loop through form fields, if one is left empty flag it as an error in formErrors state object
            if(form_details[field_key] === ""){
                tempFormErrors[field_key] = true;
                formValid = false;
            }else{
                tempFormErrors[field_key] = false;
            }
        }
        // If we have errors stop here and set the error state object.
        this.setState({formErrors: tempFormErrors});

        if(!formValid){ return; }

        //Checking for legal age as per province
        var tempFormErrors = this.state.formErrors;
        var age = this.getAge(this.state.year+"/"+this.state.month+"/"+this.state.day);

        if(this.state.province === "AB" || this.state.province === "QC"){
            if(age < 18){
                tempFormErrors.age = true;
                formValid = false;
            }else{
                tempFormErrors.age = false;
            }
        }else{
            if(age < 19){
                tempFormErrors.age = true;
                formValid = false;
            }else{
                tempFormErrors.age = false;
            }
        }
        // Set the error state object.
        this.setState({formErrors: tempFormErrors});
        
        // Check if form is valid. If so return true
        return formValid;
        
    }
    sendFormData(){        
        axios.post('/api/post.php', {data: this.state})
        .then(res => {            
            navigate("/thank-you/")
        })
    }
    isValid(field_key){
        if(this.state.formErrors[field_key]){
            return "error ";
        }else{
            return "";
        }
    }
    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }
    render() {
        return(
            <div id="entry-form" className="py-5 ">
                <form className="mb-5" onSubmit={this.handleSubmit}>
                    <img className="mw-100 mb-4 mt-5" src={white_logo} alt="Rise Up Logo"/>
                    <p className="mb-4 text-center">Believe in the power of the human spirit and join our movement.<br/>
Plus, stay up to date with all things Rise Up. </p> 
                    <p className="small"><sup>*</sup>Required Field</p>
                    <div className="form-row">
                        <div className={"col-md-6 form-group " + this.isValid("firstName")}>
                            <input type="text" className="form-control" id="lead_firstName" name="firstName" value={this.state.firstName} onChange={this.handleInputChange} />
                            <label htmlFor="lead_firstName"><sup>*</sup>First Name</label>
                            <small className="error-message"><sup>*</sup>Please enter your first name.</small>
                        </div>
                        <div className={"col-md-6 form-group " + this.isValid("lastName")}>
                            <input type="text" className="form-control" id="lead_lastName" name="lastName" value={this.state.lastName} onChange={this.handleInputChange}  />
                            <label htmlFor="lead_lastName"><sup>*</sup>Last Name</label>
                            <small className="error-message"><sup>*</sup>Please enter your last name.</small>
                        </div>
                    </div>
                    
                    <div className={"form-group " + this.isValid("email")}>
                        
                        <input type="email" className="form-control" id="lead_email" name="email" value={this.state.email} onChange={this.handleInputChange}  />
                        <label htmlFor="lead_email"><sup>*</sup>Email</label>
                        <small className="error-message"><sup>*</sup>Please enter your email name.</small>
                    </div>
                    
                    <div className="form-row">
                        <div className="col-md-6">
                            <div className={"form-group " + this.isValid("province")}>
                                <Province id="lead_province" province={this.state.province} name="province" handleInputChange={this.handleInputChange}/>
                                <label htmlFor="lead_province"><sup>*</sup>Province/Territory</label>                                
                                <small className="error-message">*Select your Province/Territory.</small>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="row">
                                <div className={"mb-2 mb-md-0 col-md-6 form-group " + this.isValid("month") + this.isValid("day") + this.isValid("year")}>
                                    <Month id="lead_month" month={this.state.month} name="month" handleInputChange={this.handleInputChange}/>
                                    <label className="d-none d-md-block" htmlFor="lead_month"><sup>*</sup>Date of Birth</label>
                                </div>
                                <div className={"mb-2 mb-md-0 col-md-3 form-group " + this.isValid("day")}>
                                    <Day id="lead_day"  month={this.state.month} day={this.state.day} name="day" handleInputChange={this.handleInputChange}/>
                                </div>
                                <div className={"mb-2 mb-md-0 col-md-3 form-group " + this.isValid("year")}>
                                    <Year id="lead_year" year={this.state.year} name="year" handleInputChange={this.handleInputChange}/>
                                    <label className="d-block d-md-none mt-2" htmlFor="lead_month"><sup>*</sup>Date of Birth</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p className={"age-message text-center "+this.isValid("age")}><small>Oops! Looks like you’re not old enough to enjoy.<br/>Come back when you’ve reached the legal age for cannabis consumption.</small></p>

                    <div className="form-group mt-4">
                        <div className="form-check">
                        <input className="form-check-input" type="checkbox" id="gridCheck" name="newsletter" value={this.state.newsletter} onChange={this.handleInputChange} />
                        <label className="form-check-label" htmlFor="gridCheck">
                        YES! I want to receive electronic messages from Rise Up. You can unsubscribe at any time.
                        </label>
                        </div>
                    </div>
                    <button type="submit" className="btn btn-primary">Subscribe</button>
                </form>
            </div>
        )
    }
}