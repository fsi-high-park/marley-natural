*Project Stack*
Build pages as a React app and export them as static HTML files through gatsby. React app queries its content from contentful. Forms submit POST request to local PHP resource. When contentful gets an update, a hook is fired to rebuild the static site on AWS.

PHP resource handles form post requests, saves content to RDS MySQL database and pushes content to Acoustic. On acoustic post success flag entry in RDS database as sent. Run check on entire database table to check for any entries that haven't been flagged as successfuly sent to acoustic.


**AWS Process**
db endpoint: tilray-subbrand-storage.cln97db9dw4v.ca-central-1.rds.amazonaws.com
username: admin_tilray
password: mbOknx1KkKlEOCa39OCP
 - Connect to tilray-subbrand-storage database on Amazon RDS
 - Create new table for brand

***Instance nameing convention***
Brand - Development
Brand - Staging
Brand - Production
(Where Brand is the brand name)

***Setup***
Development
- Create new Instance
- Choose Machine: Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type - ami-04070f04f450607dc
- Choose Type: t2.nano
Tag with brand name, & site tier (tier1, 2, or 3)
Connect to RDS Security Group Type MYSQL/Aurora 3306 : default (sg-bc28e7d6)
Launch Instance
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html
https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
~Repeat for Staging~

Production
- Create new Instance
- Choose Machine: Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type - ami-04070f04f450607dc
- Choose Type: t2.micro
Tag with brand name, & site tier (tier1, 2, or 3)
Connect to RDS Security Group Type MYSQL/Aurora 3306 : default (sg-bc28e7d6)
Launch Instance
https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-LAMP.html
https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-up-node-on-ec2-instance.html
- Document DNS (see DNS section)


**DNS**
Grab Public IP address from instances page on AWS and fill in below.

example.ca
------------------
@	A	192.168.1.1
www	CNAME	example.ca


**Libraries/Tools**
 - React
 - Gatsby
 - Bootstrap 4.x
 - NPM
 - Webpack


**Install**
Pull git repo.
Run `npm install` in folder root.

Run `gatsby develop`
Checkout site at http://localhost:8000/

Run `gatsby build` to compile static html files into public folder. (This step doesn't need to be done locally)


**Version Control**
Create git repository for brand in BitBucket.
Rename master to Production.
Push this repo starting point to the Production branch.
Create Development & Staging branches based on Production.
Push a blank index.html file to Production until site is ready to be live.
Merge Development -> Staging and Merge Staging -> Production

**Deployment**
Connect to AWS instance. 
Navigate to `cd /var/www`
Pull content from BitBucket to instance.
Each instance should only ever pull its associated branch.

Development Branch -> Brand - Development Instance
`git pull origin Development`

Staging Branch -> Brand - Staging Instance
`git pull origin Staging`

Production Branch -> Brand - Production Instance
`git pull origin Production`

Run Gatsby Build on Instance
`gatsby build`
`rm -r html`
`mv public html`

**Environment Setup**
Leave this README.md file in project root. If a project specific README is require, it should be stored in /src/ folder.
Change table name in db_config.php file to brand specific table name created earlier and set Acoustic URL to one created/provided.
	
***Code structure***

        static/ <-- Static files you want in your public folder outside the gatsby enviroment>
          api/ <-- PHP CRUD functions>        
            post.php <-- PHP script to save entries to db and acoustic>
        src/ <-- Only edit these files>
          pages/ <-- Ever js file here will render to its own URL based on file name>
          components/ <-- Any pieces of the site that needs to be reused and doesnt have its own URL>
          assets/
            images/
            sass/
              main.scss <-- Single sass file, all sass files import into here.>
        public/ <-- This is a build folder, everything here will be replaced by the gatsby build process>
        package.json
        db_config.php <-- Save AWS RDS database details here where they can't be accessed from a URL>

